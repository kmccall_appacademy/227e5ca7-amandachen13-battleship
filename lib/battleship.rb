require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("AC"), board = Board.new)
    @player = player
    @board = board
    @hit = false
  end

  def play
    until self.game_over?
      self.play_turn
    end

    puts "You win!"
  end

  def play_turn
    self.attack(@player.get_play)
    self.display_status
  end

  def count
    @board.count
  end

  def attack(pos)
    if @board[pos] == :s
      @hit = true
    end

    @board[pos] = :x
  end

  def display_status
    if @hit
      puts "It's a hit!"
      puts "There are #{count} ships remaining."
    else
      puts "It's a miss!"
    end

    @hit = false
  end

  def game_over?
    @board.won?
  end

end

if $PROGRAM_NAME == __FILE__
  game = BattleshipGame.new
  10.times {game.board.place_random_ship}
  game.play
end
