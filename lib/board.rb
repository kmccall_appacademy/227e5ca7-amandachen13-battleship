class Board

  attr_reader :grid

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    @grid[row][col] = val
  end

  def count
    @grid.flatten.count {|pos| pos == :s}
  end

  def empty?(pos = nil)
    if pos.nil? && self.count != 0
      return false
    elsif pos.nil? && self.count == 0
      return true
    else
      return self[pos] == nil
    end
  end

  def full?
    @grid.flatten.include?(nil) == false
  end

  def place_random_ship
    if self.full?
      raise "Board is full."
    end
    self[[rand(self.grid.length), rand(self.grid[0].length)]] = :s
  end

  def won?
    self.count == 0
  end

end
