class HumanPlayer

  def initialize(name)
    @name = name
  end

  def get_play
    puts "Enter a move in the format x, y."
    move = gets.chomp
    [move[0].to_i, move[-1].to_i]
  end

end
